# Maintainer: Piotr Miller <nwg.piotr@gmail.com>
# Project: nwg-shell for sway, https://github.com/nwg-piotr/nwg-shell
_git_ref=4724b52b0da
pkgname=('nwg-drawer-git')
_pkgname=${pkgname%-git}
pkgver=0.3.8.r20_pre.git.4724b52b0da
pkgrel=1
pkgdesc="Application drawer for sway and other wlroots-based compositors"
arch=('x86_64')
url="https://github.com/trinitronx/nwg-drawer"
license=('MIT')
provides=('nwg-drawer')
conflicts=('nwg-drawer')
makedepends=('go')
depends=('gtk3' 'gtk-layer-shell' 'xdg-utils')
optdepends=('alacritty: to open .desktop files with Terminal=true'
            'thunar: to open files and directories')
source=("git+https://github.com/trinitronx/${_pkgname}.git")
sha256sums=('SKIP')

pkgver() {
  cd "${srcdir}/${_pkgname}"
  git describe --long --abbrev=11 --tags | sed 's/\([^-]*\)-g/r\1_pre-g/; s/^v//; s/-/./g; s/\.g/.git./g;'
}

build() {
  cd "${srcdir}/${_pkgname}"
  export CGO_LDFLAGS="${LDFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"
  export GOFLAGS="-buildmode=pie -trimpath -mod=readonly -modcacherw -ldflags=-linkmode=external"
  export GOPATH="${srcdir}"/go
  export PATH=$PATH:$GOPATH/bin
  go build -o bin/"$_pkgname" *.go
}

package() {
  cd "$srcdir"
  install -d "$pkgdir"/usr/share/"$_pkgname"/desktop-directories
  install -Dm644 -t "$pkgdir"/usr/share/"$_pkgname"/desktop-directories/ "$_pkgname"/desktop-directories/*
  install -Dm644 -t "$pkgdir"/usr/share/"$_pkgname" "$_pkgname"/drawer.css
  install -Dm755 -t "$pkgdir"/usr/bin "$_pkgname"/bin/"$_pkgname"
}
